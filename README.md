# Web Expo pop-up

Espacio para colocar el html estático de la web https://expo.tedic.org

## Para modificar este sitio siga estas instrucciones

1. Clonar el repo: git@gitlab.com:tedicpy/expopopup.git
2. Realizar los cambios
3. Hacer git add y git commit especificando lo que se cambió
4. Por último hacer git push y avisar al admin
